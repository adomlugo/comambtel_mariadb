-- Script para el cargue de datos iniciales de la tabla DEPARTAMENTO
-- Supone que el fichero CSV está en la ruta C:\Datos del computador y que nos hemos conectado con una herramienta de terceros al servidor remoto por ejemplo el SQLYOG
LOAD DATA LOCAL INFILE 'C:\\Datos\DEPARTAMENTO.csv' 
INTO TABLE conambtel.DEPARTAMENTO 
FIELDS TERMINATED BY ';' 
OPTIONALLY ENCLOSED BY '"' 
LINES TERMINATED BY '\n' 
IGNORE 1 LINES (departamento_id, nombre);
