# conambtel_mariadb

## Descripción
Este repositorio contiene scripts para la creación del esquema físico de la base de datos de MariaDB.

## Requisitos
Se requiere acceso a una instancia en la nube o localmente de MariaDB con permisos de administrador, 
git instalado para clonar el repositorio
sqlyog community edition instalado en el computador para acceder desde el a la base de datos remota.

## Instrucciones de Implementación

clonar el repositorio
```
git clone https://gitlab.com/adomlugo/conambtel_mariadb.git
cd conambtel_mariadb
```

abrir el sqlyog y crear una conexion a la instancia de mariadb a la que se tiene acceso proporcionando el host, el puerto, el usuario y la clave
abrir la conexión y crear la base de datos con la siguiente instrucción

```
CREATE DATABASE conambtel;
```
abrir el script "creacion_bd.sql" y ejecutarlo |>

En otra pestaña abrir el script "cargar_departamentos.sql" y modificar la ruta donde se encuentra el archivo "DEPARTAMENTO.csv" descargado del repositorio y
ejecutar el script para cargar los departamentos

En otra pestaña abrir el script "cargar_ciudades.sql" y modificar la ruta donde se encuentra el archivo "CIUDAD.csv" descargado del repositorio y
ejecutar el script para cargar las ciudades

En otra pestaña abrir el script "modelo_ejemplo_empresa.sql" y modificar la sentencia SQL con el fin de crear un registro de ejemplo para una empresa.
ejecutar el script para crear este registro que tendra el id=1

En otra pestaña abrir el script "modelo_ejemplo_teletrabajador.sql" y modificar la sentencia SQL con el fin de crear un registro de ejemplo para un teletrabajador.
ejecutar el script para crear este registro que tendra el id=1

