-- crear la base de datos
CREATE DATABASE conambtel;

-- permite seleccionar la base de datos llamada conambtel;
USE conambtel;

-- crea la tabla DEPARTAMENTO;
CREATE TABLE DEPARTAMENTO (
  departamento_id INT(11) NOT NULL AUTO_INCREMENT,
  nombre VARCHAR (255) NOT NULL,
  PRIMARY KEY (departamento_id)
) ENGINE=INNODB;

-- crea la tabla CIUDAD;
CREATE TABLE CIUDAD (
  ciudad_id INT(11) NOT NULL AUTO_INCREMENT,
  departamento_id INT(11) NOT NULL,
  nombre VARCHAR(255) NOT NULL,
  PRIMARY KEY (`ciudad_id`),
  KEY departamento_id (departamento_id),
  CONSTRAINT caj_departamento FOREIGN KEY (departamento_id) REFERENCES DEPARTAMENTO (departamento_id) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=INNODB;


-- crea la tabla EMPRESA;
CREATE TABLE EMPRESA (
  empresa_id INT (11) NOT NULL AUTO_INCREMENT,
  nit VARCHAR (11) NOT NULL,
  nombre VARCHAR (255) NOT NULL,
  usuario VARCHAR (255) NOT NULL,
  contraseña VARCHAR (255) NOT NULL,
  host_correo_saliente VARCHAR (255),
  usuario_correo_saliente VARCHAR (255),
  contraseña_correo_saliente VARCHAR (255),
  de_correo_saliente VARCHAR (255),
  para_correo_saliente VARCHAR (255),
  puerto_correo_saliente INT (11),
  luz_mínima DOUBLE NOT NULL,
  luz_máxima DOUBLE NOT NULL,
  ruido_mínimo DOUBLE NOT NULL,
  ruido_máximo DOUBLE NOT NULL,
  temperatura_mínima DOUBLE NOT NULL,
  temperatura_máxima DOUBLE NOT NULL,
  distancia_ideal DOUBLE NOT NULL,
  tiempo_pausa_activa DOUBLE NOT NULL,
  espera_entre_notificaciones DOUBLE NOT NULL,
  espera_entre_envíos_a_repositorio DOUBLE NOT NULL,
  PRIMARY KEY (empresa_id)
) ENGINE= INNODB;

-- crea la tabla TELETRABAJADOR;
CREATE TABLE TELETRABAJADOR (
  teletrabajador_id INT (11) NOT NULL AUTO_INCREMENT,
  empresa_id INT (11),
  cédula VARCHAR (255) NOT NULL,
  nombre VARCHAR (255) NOT NULL,
  género VARCHAR (1) NOT NULL,
  fecha_nacimiento DATE NOT NULL,
  fecha_ingreso DATE NOT NULL,
  ciudad_id INT (11) NOT NULL,
  correo VARCHAR (255),
  PRIMARY KEY (teletrabajador_id),
  KEY caj_ciudad (ciudad_id),
  CONSTRAINT caj_empresa FOREIGN KEY (empresa_id) REFERENCES EMPRESA (empresa_id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT caj_ciudad FOREIGN KEY (ciudad_id) REFERENCES CIUDAD (ciudad_id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT CONSTRAINT_1 CHECK (género IN ('M','F')),
  CONSTRAINT CONSTRAINT_2 CHECK (fecha_nacimiento < fecha_ingreso)
) ENGINE= INNODB;

-- crea la tabla MEDICIÓN;
CREATE TABLE MEDICIÓN (
  medición_id INT (11) NOT NULL AUTO_INCREMENT,
  teletrabajador_id INT (11) NOT NULL,
  fecha_hora DATETIME NOT NULL,
  luz DOUBLE NOT NULL,
  temperatura DOUBLE NOT NULL,
  pausa_activa INT (11) NOT NULL,
  ruido DOUBLE NOT NULL,
  PRIMARY KEY (medición_id),
  KEY caj_teletrabajador (teletrabajador_id),
  CONSTRAINT caj_teletrabajador FOREIGN KEY (teletrabajador_id) REFERENCES TELETRABAJADOR (teletrabajador_id) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE= INNODB;

