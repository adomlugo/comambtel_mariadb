-- Script para el cargue de datos iniciales de la tabla CIUDAD
-- Supone que el fichero CSV está en la ruta C:\Datos del computador y que nos hemos conectado con una herramienta de terceros al servidor remoto por ejemplo el SQLYOG
LOAD DATA LOCAL INFILE 'C:\\Datos\CIUDAD.csv' 
INTO TABLE conambtel.CIUDAD 
FIELDS TERMINATED BY ';' 
OPTIONALLY ENCLOSED BY '"' 
LINES TERMINATED BY '\n' 
IGNORE 1 LINES (ciudad_id, departamento_id, nombre);
